
import React from 'react'
import PropTypes from 'prop-types'
import ReactEcharts from "echarts-for-react";

const PortfolioValueChart = props => (
    <ReactEcharts option={chart(props.tickers_symbols, props.tickers_dates, props.tickers)} style={{ height: 300 }} />
);

const mapStockValues = ({ticker_symbol: symbol, tickers: tickers}) => {
    return ({
        name: symbol,
        type: 'line',
        stack: 'portfolio',
        areaStyle: {},
        data: tickers,
        label: {
            show: true,
            position: 'insideBottomLeft'
        }
    });
};

const chart = (tickers_symbols, tickers_dates, tickers) =>
    ({
        title: {
            text: "Your portfolio value over time",
        },
        tooltip: {
            trigger: "axis",
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            },
        },
        legend: {
            orient: "horizontal",
            right: 'center',
            data: tickers_symbols
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: tickers_dates
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: tickers.map(mapStockValues)
    }
);

PortfolioValueChart.propTypes = {
    stocks_symbols: PropTypes.arrayOf(PropTypes.string),
    dates: PropTypes.arrayOf(PropTypes.string),
    stock_values: PropTypes.arrayOf(PropTypes.object)
}


export default PortfolioValueChart
