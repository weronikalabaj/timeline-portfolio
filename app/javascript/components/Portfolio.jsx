
import React from 'react'
import {Container, Row, Col} from 'react-bootstrap';
import PortfolioParamsForm from './PortfolioParamsForm';
import PortfolioValueChart from './PortfolioValueChart';
import PortfolioAllocationChart from "./PortfolioAllocationChart";

class Portfolio extends React.Component {
    constructor(props) {
        super(props);

        this.parseResponse = this.parseResponse.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.updatePortfolioParams = this.updatePortfolioParams.bind(this);

        this.state = {
            portfolio: {
                startDate: '',
                startingAmount: '',
                allocation: ''
            },
            tickers_allocations_symbols: [],
            tickers_allocations: [],
            tickers_dates: [],
            tickers_symbols: [],
            tickers: [],
            errors: ''
        }
    }

    updatePortfolioParams(field, value){
        const { portfolio } = this.state;

        this.setState({
            portfolio: Object.assign({},
                portfolio,
                {[`${field}`]: value})
        });
    }

    objectToQueryString(obj) {
        return Object.keys(obj).map(key => key + '=' + obj[key]).join('&');
    }

    handleSubmit() {
        const {portfolio} = this.state;

        const params = Object.keys(portfolio).map(key => key + '=' + portfolio[key]).join('&');

        fetch(`/portfolios/index?${params}`)
            .then(response => {
                status = response.status;
                return response.json()
            })
            .then(json => {
               if(status == 200) {
                   this.parseResponse(json);
                } else {
                   this.setState({errors: json.errors});
                }
            });
    }

    parseResponse(json){
        const tickers_symbols = Object.keys(json.portfolio_values);
        const tickers_dates = Object.keys(json.portfolio_values[tickers_symbols[0]]);
        let tickers = [];
        Object.keys(json.portfolio_values).forEach(function (symbol) {
            tickers.push({stock_symbol: symbol, tickers: Object.values(json.portfolio_values[symbol])})
        })

        const tickers_allocations_symbols = Object.keys(json.shares_allocation);
        const tickers_allocations = Object.values(json.shares_allocation);

        this.setState({
            errors: '',
            tickers_symbols: tickers_symbols,
            tickers_dates: tickers_dates,
            tickers: tickers,
            tickers_allocations_symbols: tickers_allocations_symbols,
            tickers_allocations: tickers_allocations
        });
    }

       render() {
           return (
               <Container>
                   <Row>
                       <Col>
                           <PortfolioParamsForm
                               onFieldChange={this.updatePortfolioParams}
                               onFormSubmit={this.handleSubmit}
                               porfolioParams={this.state.portfolio}
                           />
                       </Col>
                   </Row>
                   <Row>
                       <Col>
                           <div style={{ color: 'red', padding: '20px' }}>
                               <p>{this.state.errors}</p>
                           </div>
                       </Col>
                   </Row>
                   <Row>
                       <Col>
                           <PortfolioValueChart
                               tickers_dates={this.state.tickers_dates}
                               tickers_symbols={this.state.tickers_symbols}
                               tickers={this.state.tickers}
                           />
                       </Col>
                   </Row>
                   <Row>
                       <Col>
                           <PortfolioAllocationChart
                               tickers_allocations_symbols={this.state.tickers_allocations_symbols}
                               tickers_allocations={this.state.tickers_allocations}
                               start_date={this.state.portfolio.startDate}
                           />
                       </Col>
                   </Row>
               </Container>

           );
       }
}


export default Portfolio
