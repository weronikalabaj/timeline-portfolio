
import React from 'react'
import PropTypes from 'prop-types'
import ReactEcharts from "echarts-for-react";

const PortfolioAllocationChart = props => {
    return (<ReactEcharts option={chart(props.tickers_allocations_symbols, props.tickers_allocations, props.start_date)} style={{height: 300}}/>);
};

const mapSymbols = () => {
    return ({
        type: 'bar',
        label: {
            show: true,
            position: 'insideBottom'
        }
    });
};

const chart = (tickers_symbols, tickers_allocations, start_date) => {
    const dataset = [['stocks'].concat(tickers_symbols)].concat([[start_date].concat(tickers_allocations)]);

    return ({
        title: {
            text: "Your portfolio allocation over time",
        },
        tooltip: {},
        legend: {},
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {type: 'category'},
        yAxis: {},
        dataset: {
            source: dataset
        },
        series: tickers_symbols.map(mapSymbols)
    });
}

export default PortfolioAllocationChart
