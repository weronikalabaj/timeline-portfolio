
import React from 'react'
import {Form, Button} from 'react-bootstrap';


class PortfolioParamsForm extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onChange(field){
        return event => {
            this.props.onFieldChange(field, event.target.value);
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        this.props.onFormSubmit();
    }

    render() {
        const {startDate, startingAmount, allocation} = this.props.porfolioParams;

        return(
            <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="formStartDate">
                    <Form.Label>Start date</Form.Label>
                    <Form.Control type="date" onChange={this.onChange('startDate')} value={startDate} placeholder="Enter start date" required />
                </Form.Group>
                <Form.Group controlId="formStartingAmount">
                    <Form.Label>Starting amount</Form.Label>
                    <Form.Control type="number" onChange={this.onChange('startingAmount')} value={startingAmount} placeholder="Enter starting amount" required />
                </Form.Group>
                <Form.Group controlId="formPortfolioAllocation">
                    <Form.Label>Portfolio allocation</Form.Label>
                    <Form.Control type="text" onChange={this.onChange('allocation')} value={allocation} placeholder="Enter portfolio allocation, e.g. 'TWTR:0.2, GOOGL:0.8'" required />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Update charts
                </Button>
            </Form>
        );
    }
}

export default PortfolioParamsForm