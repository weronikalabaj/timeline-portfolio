require './lib/portfolios/portfolio'
require './lib/portfolios/wtd_repository'
require 'date'

class PortfoliosController < ApplicationController

  def index
    if !params[:allocation].nil?
      shares_allocation = portfolio_service.shares_allocation(params[:startDate], parse_allocation_params(params[:allocation]), params[:startingAmount].to_d)
      end_date = Date.today.to_s
      portfolio_values = portfolio_service.portfolio_values_over_time(params[:startDate], end_date, shares_allocation)

      render status: 200, json: { portfolio_values: portfolio_values, shares_allocation: shares_allocation }
    end

    rescue NoTickers => _
      render_error('No data on the specified date, use a different date or try again later.')
    rescue InvalidAllocation => _
      render_error('Invalid portfolio allocation, the sum of allocations should be equal to 1.')
    rescue InvestedAmountTooLow => _
      render_error("Invested amount is too low. You need to buy at least one of each of the chosen shares.")
    rescue => _
      render_error("Something went wrong, chase the dev to fix it.")
  end

  private

  def render_error(message)
    render status: 400, json: { errors: message }
  end

  def portfolio_service
    @portfolio_service ||= Portfolio.new(tickers_repository)
  end

  def tickers_repository
    @tickers_repository ||= WTD_repository.new
  end

  def parse_allocation_params(text)
    allocation = {}

    text.split(',').map do |stock|
      key_value = stock.split(':')
      allocation[key_value[0].strip]= key_value[1].to_d
    end

    allocation
  end
end
