## Task description

Requirements:
As a User I want a web application where I can enter the state of my assets portfolio at some day in the past and see how much money would be worth today.
Example:
Start Date: 2013-03-20
Initial Balance: $32500
Portfolio Allocation:
AAPL: 20%
GOOG: 50%
VTI: 30%
Please make a small app where the user can enter this data and can get the results. Feel free to visualize the output as you see fit (Aggregate portfolio returns chart, Portfolio Allocation evolution over time, etc).

To obtain the historical returns you can use this free API https://www.worldtradingdata.com/

Please try to show your skills both on the back-end and in the front-end.
For styles feel free to use something like normalize.css and if you want a minimal css framework. Writing your own styles is appreciated.
If you add charts, use any charting library you prefer.

## Solution overview

I wrote an app using Ruby on Rails and ReactJS. These are technologies I've been working with for the last year or so. I strived to mimic the normal "work flow" from my day to day job, but it's been different in many aspects :) 

For example, at work I haven't yet had to start a new project from scratch, we've been working with somewhat outdated libraries and our build process on the frontend needs to be replaced. 

On the other hand, I'm very familiar with the domain and code already, so I have a pretty good idea of how long something takes and how to get the first, rough version working. In case of a more complex task, I'd usually start with getting something working, ignoring how clean or not the code is, just to figure out what needs to be done. Then I'll re-start from scratch, the "clean version", according to a plan created during the exploratory phase.

I got overly optimistic after reading the suggested time to spend on the task is 6 hours :) So I've finished at that first stage, where I'd do the "mini rewrite", but I really don't have more time to work on this. So I'm not really happy with the output, I see plenty of areas of improvement, but this is where I was able to get it given the time constraints.

## How to run the code

The app is deployed to Heroku and available at the address https://damp-everglades-55623.herokuapp.com/, however I'm not certain it still works, as the API seems to be (temporarily?) returning invalid data (see [here](https://api.worldtradingdata.com/api/v1/history_multi_single_day?symbol=SNAP,TWTR&date=2019-01-03&api_token=klSODe4HkqsDHZcmffqYL5Xw9a89PnCiQlA0MopLGE7XR4seffiLNRVTqIHG)).
Source code is available at https://bitbucket.org/weronikalabaj/timeline-portfolio/.

The app is based on the standard Rails setup, so in case you don't have that ready:

- Install Ruby version 2.5.7, e.g. in the console type `rvm install ruby-2.5.7`. You can check the version with `ruby -v`.
- Install all the dependencies and build a solution using `bundle install`.
- Finally, run the app with the standard `bundle exec rails s` or `bundle exec puma`.
- To run tests type `bundle exec rake test`

Sample params that produce nice charts:

- Start date: 03/01/2019
- Invested amount: 10000
- Portfolio allocation: TWTR:0.2, GOOGL:0.5, VTI:0.3

I've handled only a few basic exceptions, such as that allocation total has to be equal to one, investment amount should be sufficient to buy at least one share of every type specified in the allocation, no data returned for a given date (or another api exception).

Important assumptions and simplifications:

- The portfolio allocation describes the proportions of invested amount that we strive to satisfy.
- The allocation values in the portfolio allocation have to sum to 1 (or in other words, to 100%).
- At least one share has to be bought for each type specified in the portfolio allocation. If the invested amount is not sufficient, we inform the user.
- If there's no data on a specified start date, we ask the user to provide a different one.
- The numbers of shares have to be natural numbers.
- The amount needed to buy the calculated number of shares will rarely match exactly the invested amount, but I just ignore what's left in the end.


## If I had more time...

Some of the things I wish I could do if I had more time to work on this:

- Review all the code after a good night's sleep. Start a "clean version" from scratch (technique learned from Corey Heines, really great!).
- I'd definitely spend a lot of time with the business expert to understand what needs to be done here and how. I'd treat this version of code as a POC level (at best), used mainly to visualize the overall approach and for me to learn about what needs to be done and what decisions need to be made. A lot of questions came up while working on this task, for example:
  - What are the standard/common algorithms for determining the number of shares based on the portfolio allocation?
  - Would we be able to give users an option to use fractional shares, or is it only a special deal offered by Vanguard (I found that info on their website)?
  - What should happen in case we're not able to match exactly the proportions of the suggested portfolio (which happens more often than not), how do we "optimize" the allocation?
  - What should happen with the remaining amount that hasn't been used for purchasing shares?
  - What vocabulary used here makes or doesn't make sense to them?
- In code:
  - Better error handling, including calculating the minimum amount needed to build the portfolio with desired allocations.
  - Better error handling for 3rd party API.
  - More tests, way more tests for all sorts of scenarios.
  - Re-consider how I'd design the PortfolioService class. It's very heavily influenced by the way 3rd party api works, I don't like that.
  - Spend a lot more time on the better setup, CI pipelline, deployment scripts (e.g. Docker for the dev enviornment) and establishing good conventions to save on time in the long run. I've used the rails webpacker with mostly default configuration to start the app, which was a kind of an overkill for what I've eventually needed. A lot of things should be removed here.
  - Refactor the code to be able to easily switch algorithms/strategies for determining the number of shares based on passed portfolio allocation.
  - Show user what amount exactly was needed to buy the shares to build the portfolio they desired and how closely it actually matched the intent.  
- Even though it's a good practice overall, in this case I wouldn't start with any sort of backend tests. I got a few things completely wrong and completely changed that part of the code once I figured what do I really need in the frontend and what I can get from the suggested data source. If I were more familiar with the problem space then it'd be ok to start with tests and backend, but in this kind of "prototype"/exploration phase starting from the UI or even "from the middle", i.e. getting a very minimal verion of all the pieces working together, would be way more productive.
- Work in smaller steps and with more minimal assumptions. Even though I've tried it, in practice I've seen how many things I've tried to do at once and how some of the steps were premature. For example, I haven't had the time to look into the second part of the task with portfolio rebalancing where the second chart would make much more sense.