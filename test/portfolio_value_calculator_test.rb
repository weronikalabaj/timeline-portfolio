require './lib/portfolios/portfolio'

class PortfolioValueCalculatorTest < MiniTest::Test
  class StockPricesMock
    def get_stock_prices_on(date, _)
      stock_prices[date]
    end

    private

    def stock_prices
      @stock_prices ||= {
          '2020-01-10' => {'AAPL': 300.00, 'GOOG': 1500.00, 'VTI': 150.00},
          '2019-12-10' => {'AAPL': 150.00, 'GOOG': 2000.00, 'VTI': 150.00},
          '2019-11-10' => {'AAPL': 150.00, 'GOOG': 1500.00, 'VTI': 150.00},
          '2020-01-01' => {}
      }
    end
  end

  def setup
    @stock_prices_mock = StockPricesMock.new
    @portfolio_allocation = {'AAPL': 0.2, 'GOOG': 0.5, 'VTI': 0.3}
    @shares_allocation_A = {'AAPL': 2, 'GOOG': 1, 'VTI': 6}
    @shares_allocation_C = {'AAPL': 4, 'GOOG': 1, 'VTI': 6}
    @stock_prices_as_of_date_A = @stock_prices_mock.get_stock_prices_on('2020-01-10', @portfolio_allocation.keys)
    @stock_prices_as_of_date_B = @stock_prices_mock.get_stock_prices_on('2019-12-10', @portfolio_allocation.keys)
    @stock_prices_as_of_date_C = @stock_prices_mock.get_stock_prices_on('2019-11-10', @portfolio_allocation.keys)
  end

  def test_exception_raised_when_invalid_portfolio_allocation
    assert_raises(InvalidAllocation) do
      Portfolio.new(@stock_prices_mock).shares_allocation('2020-01-10', {'TWTR':0.2, 'GOOGL':0.5}, 10000)
    end
  end

  def test_exception_raised_when_no_data_on_a_given_date
    assert_raises(NoTickers) do
      Portfolio.new(@stock_prices_mock).shares_allocation('2020-01-01', @portfolio_allocation, 10000)
    end
  end

  def test_exception_raised_when_invested_amount_is_too_low
    assert_raises(InvestedAmountTooLow) do
      Portfolio.new(@stock_prices_mock).shares_allocation('2019-11-10', @portfolio_allocation, 1500)
    end
  end

  def test_correct_allocation_determined
    assert_equal @shares_allocation_A, Portfolio.new(@stock_prices_mock).shares_allocation('2020-01-10', @portfolio_allocation, 3000)
    assert_equal @shares_allocation_C, Portfolio.new(@stock_prices_mock).shares_allocation('2019-11-10', @portfolio_allocation, 3000)
  end
end