class WTD_repository
  def initialize
    @api_token = 'klSODe4HkqsDHZcmffqYL5Xw9a89PnCiQlA0MopLGE7XR4seffiLNRVTqIHG'
  end

  def get_stock_prices_on(date, tickers_symbols)
    tickers = {}

    tickers_symbols.each_slice(2) do |slice|
      params = "symbol=#{slice.join(',')}&date=#{date}&api_token=#{@api_token}"
      url = "https://api.worldtradingdata.com/api/v1/history_multi_single_day?#{params}"

      response = HTTParty.get(url)

      slice.each do |symbol|
        tickers[symbol] = response['data']["#{symbol}"]['close'].to_d
      end
    end

    tickers

  rescue => _
    raise NoTickers.new
  end

  def get_stock_prices_between(startDate, endDate, ticker_symbol, qty)
    params = "symbol=#{ticker_symbol}&date_from=#{startDate}&date_to=#{endDate}&api_token=#{@api_token}"
    url = "https://api.worldtradingdata.com/api/v1/history?#{params}"

    response = HTTParty.get(url)

    tickers = {}

    response['history'].keys.each do |date|
      tickers[date] = response['history']["#{date}"]['close'].to_d * qty
    end

    tickers
  end
end