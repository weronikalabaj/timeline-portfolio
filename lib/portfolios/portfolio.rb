require 'json'

class InvalidAllocation < StandardError; end
class InvestedAmountTooLow < StandardError; end
class NoTickers < StandardError; end

class Portfolio
  def initialize(stock_prices_repository)
    @stock_prices_repository = stock_prices_repository
  end

  def portfolio_values_over_time(start_date, end_date, allocation)
    ticker_symbols = allocation.keys

    results = {}
    ticker_symbols.each do |symbol|
      tickers = @stock_prices_repository.get_stock_prices_between(start_date, end_date, symbol, allocation[symbol])
      results["#{symbol}"] = tickers
    end

    results
  end

  def shares_allocation(start_date, portfolio_allocation, invested_amount)
    if(portfolio_allocation.values.sum != 1)
      raise InvalidAllocation.new
    end

    ticker_symbols = portfolio_allocation.keys
    stock_prices = @stock_prices_repository.get_stock_prices_on(start_date, ticker_symbols)

    if stock_prices.blank?
      raise NoTickers.new
    end

    allocation = portfolio_allocation.map { |symbol, percentage| [symbol, Integer(invested_amount * percentage / stock_prices[symbol])]}.to_h

    if(allocation.values.any? {|qty| qty == 0})
      raise InvestedAmountTooLow.new
    end

    allocation
  end
end

